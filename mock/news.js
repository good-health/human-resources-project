module.exports = [
  {
    url: '/news/getList',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: { a: 1, b: 2, list: ['第一条', '第2条', '第3条'] }
      }
    }
  }
]
